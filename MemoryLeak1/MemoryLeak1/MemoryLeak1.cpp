// MemoryLeak1.cpp : Defines the entry point for the console application.
//
// Memory leak discussion part 1 

#include "stdafx.h"
#include "iostream"
#include "MyClass.h"
using namespace std;



int main()
{
	int i = 10;
	while (i)
	{
		cout << "\n Enter 0 to exit an infitinite loop. Press non zero  to continue creating temporary stack object !! ";
		cin >> i;
		if (i)
		{
			MyClass *myClassObj = new MyClass();
			myClassObj->doSomeWork();
			delete myClassObj;
		}
	}
    return 0;
}

