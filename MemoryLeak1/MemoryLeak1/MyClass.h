#ifndef MYCLASS_H
#define MYCLASS_H

#include <vector>

class student
{
	char *name;
	int rollNo;
public:
	student();
	~student();
};

class MyClass
{
	std::vector<student* > vecStudents;
	student * studentArray;
public:
	MyClass();
	~MyClass();
	void  doSomeWork();
};

#endif